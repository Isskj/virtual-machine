#!/bin/bash

source "`dirname $0`/common.sh"
source "`dirname $0`/download_iso.conf"
iso_filename="${iso_filepath##*/}"
md5_filename="${md5_filepath##*/}"

#------------------------------------------------
# main
#------------------------------------------------
check_command "wget"
check_command "md5sum"
verbose_exec "rm -f ${md5_filename}"

if ! [ -f ${iso_filename} ]; then
    echo "not downloaded: [${iso_filename}]"
    verbose_exec "wget ${iso_filepath} -O ${iso_filename}"
else
    echo "downloaded: [${iso_filename}]"
fi

verbose_exec "wget ${md5_filepath} -O ${md5_filename}"

checksum1=`md5sum ${iso_filename} | awk '{ print $1 }'`
checksum2=`cat ${md5_filename} | grep ${iso_filename} | awk '{ print $1 }'`

echo -e "md5checksum expected: ${checksum2}"
echo -e "md5checksum   result: ${checksum1}"

if [ ${checksum1} = ${checksum2} ]; then
    echo "md5 checksum verified." && exit 0
else
    echo "md5 checksum not verified." && exit 1
fi
