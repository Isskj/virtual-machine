#!/bin/bash

source "`dirname $0`/common.sh"
source "`dirname $0`/create_hd.conf"
source "`dirname $0`/qemu_run.conf"

harddisk="`pwd`/${harddisk}"

#------------------------------------------------
# main 
#------------------------------------------------
QEMU=qemu-system-x86_64
check_command "${QEMU}"
verbose_exec "${QEMU} ${vga_options} ${qemu_memoryopts} ${qemu_netopts} -hda ${harddisk}"
