# 簡易仮想マシーン構築手順

## 準備
Mac OSの場合、ターミナルを開き、`qemu`, `wget`をインストール
```
$ brew install qemu wget
```

## ハードディスクを作成
```
$ ./qemu/create_hd.sh
```
設定ファイル: `./qemu/create_hd.conf`

## インストーラーをダウンロード
```
$ ./qemu/download_iso.sh
```
設定ファイル: `./qemu/download_iso.conf`

## インストーラーを起動
```
$ ./qemu/install.sh
```
設定ファイル: `./qemu/qemu_run.conf`

## Debian起動
```
$ ./qemu/run.sh
```
設定ファイル: `./qemu/qemu_run.conf`
