#!/bin/bash

source "`dirname $0`/common.sh"
source "`dirname $0`/download_iso.conf"
source "`dirname $0`/create_hd.conf"
source "`dirname $0`/qemu_run.conf"

iso_filename="${iso_filepath##*/}"

install_iso="`pwd`/${iso_filename}"
harddisk="`pwd`/${harddisk}"

#------------------------------------------------
# main 
#------------------------------------------------
QEMU=qemu-system-x86_64
check_command "${QEMU}"
verbose_exec "${QEMU} ${qemu_netopts} ${qemu_memoryopts} -cdrom ${install_iso} -hda ${harddisk} -boot d"
