# Apache2, php, Mariadb

## install web server
```
$ sudo apt install apache php mariadb-server git
```

## setup mariadb
```
$ mysql_secure_installation
```

### create a user and his permission
```
$ MariaDB [()]: create user {username} identified by '{password}';
$ MariaDB [()]: grant all privileges on *.* to {username};
$ MariaDB [()]: flush privileges;
$ MariaDB [()]: grants for {username};
```
[https://mariadb.com/kb/en/grant/](https://mariadb.com/kb/en/grant/)

### create a database
```
$ mysql -u {username} -p
$ MariaDB [({username})]: create database {dbname};
$ MariaDB [({username})]: use {dbname};
$ MariaDB [({username})]: create table {tablename} ( ....
```
[https://mariadb.com/kb/en/create/](https://mariadb.com/kb/en/create/)
