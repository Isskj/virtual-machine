#!/bin/bash

#------------------------------------------------
# functions
#------------------------------------------------
verbose_exec_failskip()
{
    echo "$1" && eval "$1"
}

verbose_exec()
{
    echo "$1" && eval "$1"
    if [ "$?" != "0" ]; then
        exit 1
    fi
}

check_command()
{
    which $1
    if [ "$?" != "0" ]; then
        echo "[$1] is not installed."
        exit 1
    fi
}

