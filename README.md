# Simple setup virtual machine with qemu

## prerequisites
```
$ brew install qemu wget
```

## create hard disk
```
$ ./qemu/create_hd.sh
```
configuration file: `./qemu/create_hd.conf`

## download debian installer
```
$ ./qemu/download_iso.sh
```
configuration file: `./qemu/download_iso.conf`

## run debian installer
```
$ ./qemu/install.sh
```
configuration file: `./qemu/install.conf`

## run debian
```
$ ./qemu/run.sh
```
configuration file: `./qemu/run.conf`
