#!/bin/bash

source "`dirname $0`/common.sh"
source "`dirname $0`/create_hd.conf"

#------------------------------------------------
# main 
#------------------------------------------------
check_command "qemu-img"
verbose_exec "qemu-img create -f ${filetype} ${harddisk} ${filesize}"
