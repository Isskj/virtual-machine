# Setup ssh connections

## change sshd_config in quest os (debian in qemu)
```
~~~ in /etc/ssh/sshd_config
PasswordAuthentication yes <--- comment out
~~~

# enable password authentication, then 
$ service ssh restart
```

## ssh login from host os (your os)
```
$ ssh -p 10022 {yourname}@localhost
```

## copy your public key in host to guest os (debian in qemu)
```
## host os --------------
$ scp -P 10022 ~/.ssh/id_rsa.pub {yourname}@localhost:~/

## guest debian os ------
## generate ssh key files
$ ssh-keygen

## register host's public key in guest ssh authorized keys
$ cat id_rsa.pub >> ~/.ssh/authorized_keys
$ chown 600 ~/.ssh/authorized_keys
$ rm id_rsa.pub

## disable ssh password authentications
~~~ in /etc/ssh/sshd_config
# PasswordAuthentication yes <--- comment
~~~
# disable password authentication, then 
$ service ssh restart
```
ref: [https://www.debian.org/devel/passwordlessssh](https://www.debian.org/devel/passwordlessssh)

## ssh login from host os (your os) without password
```
$ ssh -p 10022 {yourname}@localhost
```
